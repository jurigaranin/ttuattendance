﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DAL.Entities;
using System.Data.Entity;

namespace Attendance.DAL.EF
{
    public class AttendanceContext : DbContext
    {
        
        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Presence> Presences { get; set; }

        static AttendanceContext()
        {
            Database.SetInitializer<AttendanceContext>(new AttendanceDbInitializer());
        }

        public AttendanceContext(string connectionString) : base(connectionString)
        {

        }
    }
}
