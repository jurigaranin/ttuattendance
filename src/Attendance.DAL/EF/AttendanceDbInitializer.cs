﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using Attendance.DAL.Entities;

namespace Attendance.DAL.EF
{
    public class AttendanceDbInitializer : DropCreateDatabaseIfModelChanges<AttendanceContext>
    {
        protected override void Seed(AttendanceContext db)
        {
            db.Students.Add(new Student() {Id = "143231", FirstName = "Juri", Lastname = "Garanin", Password = "1234" });
            db.SaveChanges();
        }
    }
}
