﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Attendance.DAL.Entities
{
    public class Student
    {
        public string Id { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }

        public string GroupId { get; set; }
        public Group Group { get; set; }

        public virtual ICollection<Presence> Presences { get; set; }

        public Student()
        {
            Presences = new List<Presence>();
        }
    }
}
