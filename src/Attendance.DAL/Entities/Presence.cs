﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Attendance.DAL.Entities
{
    public class Presence
    {
        public Guid Id { get; set; }
        public string Status { get; set; }
        public DateTime Date { get; set; }
        public int LessonNumber { get; set; }

        public Guid CourseId { get; set; }
        public Course Course { get; set; }

        public virtual ICollection<Student> Students { get; set; }

        public Presence()
        {
            Students = new List<Student>();
        }

    }
}
