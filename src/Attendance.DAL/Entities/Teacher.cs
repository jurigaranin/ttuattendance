﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Attendance.DAL.Entities
{
    public class Teacher
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Pasword { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public Teacher()
        {
            Courses = new List<Course>();
        }
    }
}
