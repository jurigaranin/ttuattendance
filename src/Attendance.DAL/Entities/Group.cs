﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Attendance.DAL.Entities
{
    public class Group
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Course> Courses { get; set; }

        public Group()
        {
            Students = new List<Student>();
            Courses = new List<Course>();
        }
    }
}
